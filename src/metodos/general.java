/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metodos;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Ginett
 */
public class general {

    public static boolean isValidarTamaño(String palabra) {
        boolean isValido = true;
        int len = palabra.length();
        if (len > 5) {
            isValido = false;
        }
        return isValido;
    };
    
    public static boolean isValidarLetras(String palabra) {
        boolean isValido = false;
        Pattern patron = Pattern.compile("[^A-Za-z ]");
        Matcher letras = patron.matcher(palabra);
        if(!letras.find()){
            isValido = true;            
        }
        return isValido;
    };
    
    public static boolean validarCadena(String cadena, String base){
        Boolean existe = true;
        HashMap baseLetras = new HashMap();
        for(int i=0;i<base.length();i++){
            baseLetras.put(i,base.charAt(i));
        }
        
        for(int e = 0; e < cadena.length(); e++){
            if(!baseLetras.containsValue(cadena.charAt(e))){
                existe = false;
                break;
            }            
        }
        
        return existe;
    };

}
