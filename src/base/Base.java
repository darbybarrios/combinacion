/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base;

import java.io.InputStream;
import java.util.Scanner;
import metodos.general;

/**
 *
 * @author Ginett
 */
public class Base {

    private static InputStream stream;
    private static Scanner scanner;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        stream = System.in;
        scanner = new Scanner(stream);
        String input = input();
        String cadena1 = cadena("1");
        String cadena2 = cadena("2");
        String cadena3 = cadena("3");
        System.out.println("Cadena Base: " + input);
        System.out.println("Cadena 1: " + cadena1 +" "+ general.validarCadena(cadena1, input));
        System.out.println("Cadena 2: " + cadena2 +" "+ general.validarCadena(cadena2, input));
        System.out.println("Cadena 3: " + cadena3 +" "+ general.validarCadena(cadena3, input));
        scanner.close();
    }

    public static String input() {
        System.out.print("Cadena Base: ");
        
        String base = "";
        try {
            base = scanner.next();
            if (!general.isValidarTamaño(base)) {
                System.out.println("El tamaño es mayor a 5");
                base = input();
            }
            if (!general.isValidarLetras(base)) {
                System.out.println("La cadena solo debe contener letras de la a-z");
                base = input();
            }

        } catch (Exception e) {
            e.printStackTrace();
        } 
        return base;
    }

    public static String cadena(String num) {
        System.out.print("Cadena " + num + ": ");
        
        String cadena = "";
        try {
            cadena = scanner.next();
            if (!general.isValidarLetras(cadena)) {
                System.out.println("La cadena solo debe contener letras de la a-z");
                cadena = cadena(num);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } 
        return cadena;
    }
    
   
    

}
