/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metodos;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ginett
 */
public class generalTest {
    
    public generalTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of isValidarTamaño method, of class general.
     */
    @Test
    public void testIsValidarTamaño() {
        System.out.println("isValidarTamaño");
        String palabra = "asd";
        boolean expResult = true;
        boolean result = general.isValidarTamaño(palabra);
        // TODO review the generated test code and remove the default call to fail.
        if(result != expResult ){
            fail("El tamaño es mayor a 5");
        }
        
    }

    /**
     * Test of isValidarLetras method, of class general.
     */
    @Test
    public void testIsValidarLetras() {
        System.out.println("isValidarLetras");
        String palabra = "asd";
        boolean expResult = true;
        boolean result = general.isValidarLetras(palabra);
        if(result != expResult ){
            fail("La cadena solo debe contener letras de la a-z");
        }
    }

    /**
     * Test of validarCadena method, of class general.
     */
    @Test
    public void testValidarCadena() {
        System.out.println("validarCadena");
        String cadena = "asd";
        String base = "asd";
        boolean expResult = true;
        boolean result = general.validarCadena(cadena, base);
        if(result != expResult ){
            fail("La cadena no contiene combinacion con la base");
        }
    }
    
}
